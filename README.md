# Lucia
Static website and gallery of Lucia Salinas. Highlithing the usage of Grid system and layout.

### Requisites
Basic type website in [HTML](https://developer.mozilla.org/en-US/docs/Web/HTML), [CSS](https://en.wikipedia.org/wiki/Cascading_Style_Sheets), and [Javascript](https://developer.mozilla.org/bm/docs/Learn/Getting_started_with_the_web/JavaScript_basics) to manipulate web contents without server or databases to add dynamic functionality. Please view this [link](https://en.wikipedia.org/wiki/Static_web_page) for reference.

### Installing
Employ [HTML5 Boilerplate](https://html5boilerplate.com/) for general structure of the web pages.

Use both the [CDN](https://getbootstrap.com/docs/4.2/getting-started/introduction/) and manual install Bootstrap v4.2. Refer to this [link](https://getbootstrap.com/docs/4.2/getting-started/download/) for installation.

Use [W3CSS](https://www.w3schools.com/w3css/)(CDN) in some of the css styling.

Implement the [Owl.Carousel.JS](https://owlcarousel2.github.io/OwlCarousel2/) by manually install the version 2.3.4 for Carousel image gallery.


### Deployment
Deployed live under free web hosting by [Bitbucket.org](https://bitbucket.org/product).


### Coder
[Arnel "Unkle" Imperial](https://arnelimperial.bitbucket.io)








